// This file contains all code for physics
window.Physics = ( function () {
	var m_objects;

	m_objects = [];
	m_planets = [];

	return {
		"Reset": Reset,
		"AddObject": AddObject
	};

	function Reset() {
		m_objects = [];
		m_planets = [];
	}

	function AddObject( obj, isPlanet ) {
		if( isPlanet ) {
			m_planets.push( obj );
		} else {
			obj.vx = 0;
			obj.vy = 0;
			obj.ax = 0;
			obj.ay = 0;
			m_objects.push( obj );
		}
	}

} )();