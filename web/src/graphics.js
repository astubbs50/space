// This file contains all code for rendering graphics to the screen
window.Graphics = ( function () {
	var m_canvas, m_context, m_camera, m_images, m_sprites, m_imagesLoaded;

	return {
		"Init": Init,
		"Draw": Draw,
		"AddSprite": AddSprite,
		"RemoveSprite": RemoveSprite,
		"GetCamera": GetCamera
	};

	function Init() {

		// Setup the canvas
		m_canvas = document.getElementById( "gameCanvas" );
		m_context = m_canvas.getContext( "2d" );

		// Setup the camera
		m_camera = {
			"x": 0,
			"y": 0,
			"zoom": 1,
			"antiZoom": 1
		};

		// Set the initial size
		resize();

		// Translate to center of the screen
		m_context.translate( m_camera.halfWidth, m_camera.halfHeight );

		// Setup sprites
		m_images = [];
		m_sprites = [];

		loadImages();
	}

	function Draw( dt ) {
		var i, sprite;

		// Clear the screen
		m_context.clearRect(
			-m_camera.halfWidth, -m_camera.halfWidth, m_camera.width, m_camera.height
		);

		// Scale the screen
		m_context.scale( m_camera.zoom, m_camera.zoom );

		// Translate to camera
		m_context.translate( -m_camera.x, -m_camera.y );

		// Draw all the sprites
		for( i = 0; i < m_sprites.length; i++ ) {
			sprite = m_sprites[ i ];
			m_context.translate( sprite.x, sprite.y );
			m_context.rotate( sprite.angle );
			m_context.drawImage(
				sprite.images[ sprite.frame ], -sprite.halfWidth, -sprite.halfHeight
			);
			m_context.rotate( -sprite.angle );
			m_context.translate( -sprite.x, -sprite.y );

			// Update animation frame
			updateAnimation( sprite, dt );
		}

		// Reset translation
		m_context.translate( m_camera.x, m_camera.y );

		// Reverse the scale
		m_context.scale( m_camera.antiZoom, m_camera.antiZoom );
	}

	function AddSprite( images, x, y, animations, animationIndex ) {
		var sprite, img;

		// Set default for animations
		if( animations == null ) {
			animations = [ {
				"frames": [ 0 ],
				"speed": 9999990,
				"index": 0
			} ];
		}

		// Set default for animationIndex
		if( animationIndex == null ) {
			animationIndex = 0;
		}

		// Convert images from ids to image data
		for( i = 0; i < images.length; i++ ) {
			images[ i ] = m_images[ images[ i ] ];
		}

		img = images[ 0 ];
		sprite = {
			"id": m_sprites.length,
			"images": images,
			"frame": 0,
			"animationIndex": animationIndex,
			"animations": animations,
			"frameDuration": 0,
			"x": x,
			"y": y,
			"width": img.width,
			"height": img.height,
			"halfWidth": img.width / 2,
			"halfHeight": img.height / 2
		};
		m_sprites.push( sprite );

		return sprite;
	}

	function RemoveSprite( id ) {
		var i;
		for( i = 0; i < m_sprites.length; i++ ) {
			if( m_sprites[ i ].id === id ) {
				m_sprites.splice( i, 1 );
				return true;
			}
		}
		return false;
	}

	function GetCamera() {
		return Util.CopyObject( m_camera );
	}

	function loadImages() {
		var i, image;

		m_imagesLoaded = 0;
		for( i = 0; i < g_IMAGES.length; i++ ) {
			image = new Image();
			image.src = "img/" + g_IMAGES[ i ];
			image.onload = imagesLoaded;
			m_images.push( image );
		}
	}

	function imagesLoaded() {
		m_imagesLoaded += 1;
		if( m_imagesLoaded === g_IMAGES.length ) {
			Game.ItemLoaded( "images" );
		}
	}

	function resize() {

		// Set the canvas
		m_canvas.width = m_canvas.offsetWidth;
		m_canvas.height = m_canvas.offsetHeight;

		// Setup the camera
		m_camera.width = m_canvas.width;
		m_camera.height = m_canvas.height;
		m_camera.halfWidth = m_canvas.width / 2;
		m_camera.halfHeight = m_canvas.height / 2;
	}

	function updateAnimation( sprite, dt ) {
		var animation;

		animation = sprite.animations[ sprite.animationIndex ];
		sprite.frameDuration += dt;
		if( sprite.frameDuration >= animation.speed ) {
			sprite.frameDuration = 0;
			animation.index += 1;
			if( animation.index > animation.frames.length ) {
				animation.index = 0;
			}
			sprite.frame = animation.frames[ animation.index ];
		}
	}

} )();
