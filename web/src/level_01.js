var g_LEVEL_01 = {
	"planets": [{
		"name": "Sun",
		"x": -500,
		"y": -500,
		"image": 3,
		"radius": 800,
		"mass": 50000,
		"color": "orange",
		"orbit": {
			"index": 0,
			"angle": 0,
			"speed": 0
		}
	}],
	"ships": [{
		"x": 0,
		"y": 0,
		"color": "white",
		"image": 9
	}]
};