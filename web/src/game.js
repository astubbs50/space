// This file is the games main controller, loads all resources and controls the start menu.
window.Game = ( function () {
	var m_gameLoadStatus;

	m_gameLoadStatus = {
		"images": false
	};

	window.addEventListener( "load", init );

	return {
		"ItemLoaded": ItemLoaded
	};

	function ItemLoaded( status ) {
		if( status === "images" ) {
			m_gameLoadStatus[ status ] = true;
		}

		if( m_gameLoadStatus[ "images" ] ) {
			intro();
		}
	}

	function init() {
		Graphics.Init();
		document.getElementById( "startButton" ).addEventListener( "click", startGame );
	}

	function intro() {
		Level.CreateStars();
		Graphics.Draw();
		setTimeout( function () {
			document.querySelector( "h1" ).style.fontSize = "150px";
			document.querySelector( "h2" ).style.fontSize = "90px";
			setTimeout( function () {
				document.querySelector( "div.intro" ).style.opacity = "1";
			}, 300 );
		}, 500 );
	}

	function startGame() {
		document.querySelector( "#divIntro" ).style.display = "none";
		Level.StartLevel( g_LEVEL_01 );
	}

} )();
