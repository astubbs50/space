// This file controls the individual level and all major game components
window.Level = ( function () {
	var STAR_DENSITY, m_objects, m_planets, m_time, m_level;

	STAR_DENSITY = 0.0005;		// Stars per pixels squared
	m_objects = [];
	m_planets = [];
	m_ships = [];

	return {
		"CreateStars": CreateStars,
		"StartLevel": StartLevel
	};

	function CreateStars() {
		var camera, starCount, i, starImage, x, y, starSprite;

		camera = Graphics.GetCamera();
		starCount = Math.round( camera.width * camera.height * 1.5 * STAR_DENSITY );
		for( i = 0; i < starCount; i++ ) {
			starImage = Math.floor( Math.random() * 10 );
			if( starImage >= 3 ) {
				starImage = 0;
			}
			x = Math.floor( Math.random() * camera.width * 2 ) - camera.halfWidth;
			y = Math.floor( Math.random() * camera.height * 2 ) - camera.halfHeight;
			starSprite = Graphics.AddSprite( [ starImage ], x, y );
			starSprite.angle = Math.random() * Math.PI * 2;
			starSprite.isStar = true;
			m_objects.push( starSprite );
		}
	}

	function StartLevel( level ) {
		var i, planet, ship;

		m_time = ( new Date() ).getTime();
		resetLevel();
		m_level = level;

		// Load Planets
		for( i = 0; i < m_level.planets.length; i++ ) {
			planet = m_level.planets[ i ];
			createPlanet( planet.image, planet.x, planet.y );
		}

		// Load Ships
		for( i = 0; i < m_level.ships.length; i++ ) {
			ship = m_level.ships[ i ];
			createShip( ship.image, ship.x, ship.y );
		}

		requestAnimationFrame( run );
	}

	function createPlanet( image, x, y ) {
		var planetObj;

		planetObj = Graphics.AddSprite( [ image ], x, y );
		Physics.AddObject( planetObj, true );
		m_planets.push( planetObj );
		m_objects.push( planetObj );
	}

	function createShip( image, x, y ) {
		var shipObj;

		shipObj = Graphics.AddSprite( [ image ], x, y );
		Physics.AddObject( shipObj, false );

		m_ships.push( shipObj );
		m_objects.push( shipObj );
	}

	function resetLevel() {
		var i;
		for( i = m_objects.length - 1; i >= 0; i-- ) {
			if( !m_objects[ i ].isStar ) {
				Graphics.RemoveSprite( m_objects[ i ].id );
				m_objects.splice( i, 1 );
			}
		}
		m_ships = [];
		m_planets = [];
		Physics.Reset();
	}

	function run() {
		var dt;

		dt = calcTime();
		Graphics.Draw( dt );
		requestAnimationFrame( run );
	}

	function calcTime() {
		var dt, time;
		time = ( new Date() ).getTime();
		dt = time - m_time;
		m_time = time;

		return dt;
	}

} )();
