"use strict";

if ( ! Array.isArray ) {
	Array.isArray = function( arg ) {
		return Object.prototype.toString.call( arg ) === '[object Array]';
	};
}

var Util = ( function () {

	return {
		"CopyObject": CopyObject
	};

	function CopyObject( src, dest ) {
		var prop;

		if( dest == null ) {
			dest = {};
		}

		for( prop in src ) {
			if( src.hasOwnProperty( prop ) ) {
				dest[ prop ] = src[ prop ];
			}
		}

		return dest;
	}

} ) ();
