function CreateStar()
{
	var star = {};
	star.x = Math.random() * starFieldWidth + ships[0].x - starFieldHalfWidth;
	star.y = Math.random() * starFieldHeight + ships[0].y - starFieldHalfHeight;
	var c = Math.random() * 150 + 100;
	star.r = c / 3;
	star.g = c / 2;
	star.b = c;
	
	star.color = "rgb(" + Math.round(star.r) + "," + Math.round(star.g) + "," + Math.round(star.b) + ")";
	star.size = Math.round(Math.random() * 5);
	
	//if(star.size >= 3)
	//	star.draw = DrawStar1;
	//else if(star.size > 1)
	//	star.draw = DrawStar2;
	//else
		star.draw = DrawStar3;
		
	return star;
}

function InitStars()
{
	stars = [];
	for(var i = 0; i < STAR_DENSITY; i++)
	{
		stars.push(CreateStar());
	}
}

function DrawStar1()
{
	var x = this.x;
	var y = this.y;
	var c = this.b;
	var radius = this.size;
	
	context.save();
	
	context.beginPath();
	context.moveTo(x, y);
	context.lineTo(x, y + radius + 2);
	context.lineTo(x, y - radius - 2);
	context.moveTo(x, y);
	context.lineTo(x - radius - 2, y);
	context.lineTo(x + radius + 2, y);
	context.strokeStyle = "RGB(" + 
			c/3 + ", " +
			c/2 + ", " +
			c + ")";
	context.stroke();
	
	for(var r = radius; r > 0; r -= 1)
	{
		context.beginPath();
		
		context.fillStyle = "RGB(" + 
			c/2 + ", " +
			c/2 + ", " +
			c + ")";
		context.strokeStyle = context.fillStyle;
		c += 10;
		//context.moveTo(x + r, y + r);
		context.arc(x, y, r, 0, Math.PI * 2, false);
		context.stroke();
		context.fill();
		
		x += radius / 100;
		y += radius / 100;
		if (c > 255)
			c = 225;
	}
	
	context.restore();	
}

function DrawStar2()
{
	var x = this.x;
	var y = this.y;
	var c = this.b;
	var radius = this.size;
	
	context.save();
	
	context.beginPath();
	context.moveTo(x, y);
	context.lineTo(x, y + radius + 2);
	context.lineTo(x, y - radius - 2);
	context.moveTo(x, y);
	context.lineTo(x - radius - 2, y);
	context.lineTo(x + radius + 2, y);
	context.strokeStyle = "RGB(" + 
			c/3 + ", " +
			c/2 + ", " +
			c + ")";
	context.stroke();
		
	context.restore();
}

function DrawStar3()
{
	var x = this.x;
	var y = this.y;
	var c = this.color;
	var radius = this.size;
	
	context.save();	
	context.beginPath();
	context.rect(x, y, radius, radius);
	//context.fillStyle = "red";
	context.fillStyle = c;
	context.fill();
	context.restore();
}

function BindStar(star)
{
	if(star.x > ships[0].x + starFieldHalfWidth)
	{
		star.x = ships[0].x - starFieldHalfWidth + Math.random() * ships[0].speed;
		star.y = Math.random() * starFieldHeight + ships[0].y - starFieldHalfHeight;
		star.size = Math.round(Math.random() * 5);
	}
	else if (star.x < ships[0].x - starFieldHalfWidth)
	{
		star.x = ships[0].x + starFieldHalfWidth - Math.random() * ships[0].speed;
		star.y = Math.random() * starFieldHeight + ships[0].y - starFieldHalfHeight;
		star.size = Math.round(Math.random() * 5);
	}
	
	if(star.y > ships[0].y + starFieldHalfHeight)
	{
		star.x = Math.random() * starFieldWidth + ships[0].x - starFieldHalfWidth;
		star.y = ships[0].y - starFieldHalfHeight + Math.random() * ships[0].speed;	
		star.size = Math.round(Math.random() * 5);
	}
	else if (star.y < ships[0].y - starFieldHalfHeight)
	{
		star.x = Math.random() * starFieldWidth + ships[0].x - starFieldHalfWidth;
		star.y = ships[0].y + starFieldHalfHeight - Math.random() * ships[0].speed;
		star.size = Math.round(Math.random() * 5);
	}
}

function DrawStars()
{
	for(var i = 0; i < stars.length; i++)
	{
		BindStar(stars[i]);
		stars[i].draw();
	}
}
