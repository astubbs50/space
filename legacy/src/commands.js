function RunActions()
{
	for(var i = 0; i < ships.length; i++)
	{
		ships[i].accX = 0;
		ships[i].accY = 0;
		ships[i].actionAI();
		ships[i].actionFireBullets();
		ships[i].actionFireMissile();
		ships[i].actionRotateLeft();
		ships[i].actionRotateRight();
		ships[i].actionThrust();
	}	
}


function RotateLeft()
{
	this.angle -= this.angleDelta;
	//this.angle = this.angle % TWOPI;
	if(this.angle < 0)
		this.angle = TWOPI - this.angle;
}

function RotateRight()
{
	this.angle += this.angleDelta;
	this.angle = this.angle % TWOPI;
}

function Thrust()
{
	var acc = this.thrust / this.mass;
	this.accX = acc * Math.cos(this.angle);
	this.accY = acc * Math.sin(this.angle);
	
	this.thrustAnimTime--;
	if(this.thrustAnimTime == 0)
	{			
		this.thrustAnimTime = this.thrustAnimRate;
		this.img = this.images[this.thrustAnimFrame];
		
		this.thrustAnimFrame += this.thrustAnimDirection;	
		if(this.thrustAnimFrame > this.images.length - 1 || this.thrustAnimFrame < this.thrustAnimFrameStart)
		{
			this.thrustAnimDirection *= -1;
			this.thrustAnimFrame += this.thrustAnimDirection;
		}
	}
}

function FireBullets()
{	
	this.fireTime--;
	if(this.fireTime == 0)
	{
		this.fireTime = this.bullet.rate;
		var bullet = {};	
		bullet.index = this.bullet.index;
		bullet.color = this.bullet.color;
		bullet.angle = this.angle;
		bullet.speed = this.bullet.speed;
		bullet.vx = this.vx + Math.cos(bullet.angle) * bullet.speed;
		bullet.vy = this.vy + Math.sin(bullet.angle) * bullet.speed;
		bullet.distance = this.bullet.distance;
		bullet.size = this.bullet.size;
		bullet.halfSize = this.bullet.halfSize;	
		bullet.x = this.x - bullet.halfSize + Math.cos(bullet.angle + HALFPI) * (this.img.halfWidth - 8);
		bullet.y = this.y - bullet.halfSize + Math.sin(bullet.angle + HALFPI) * (this.img.halfWidth - 8);
		bullet.x += Math.cos(bullet.angle) * (this.img.halfWidth);
		bullet.y += Math.sin(bullet.angle) * (this.img.halfWidth);
		bullet.x += bullet.vx;
		bullet.y += bullet.vy;
		bullets.push(bullet);
		
		var bullet2 = {};	
		bullet2.index = this.bullet.index;
		bullet2.color = this.bullet.color;
		bullet2.angle = this.angle;
		bullet2.speed = this.bullet.speed;
		bullet2.vx = this.vx + Math.cos(bullet2.angle) * bullet2.speed;
		bullet2.vy = this.vy + Math.sin(bullet2.angle) * bullet2.speed;
		bullet2.distance = this.bullet.distance;
		bullet2.size = this.bullet.size;
		bullet2.halfSize = this.bullet.halfSize;	
		bullet2.x = this.x - bullet2.halfSize - Math.cos(bullet2.angle + HALFPI) * (this.img.halfWidth - 8);
		bullet2.y = this.y - bullet2.halfSize - Math.sin(bullet2.angle + HALFPI) * (this.img.halfWidth - 8);
		bullet2.x += Math.cos(bullet2.angle) * (this.img.halfWidth);
		bullet2.y += Math.sin(bullet2.angle) * (this.img.halfWidth);
		bullet2.x += bullet2.vx;
		bullet2.y += bullet2.vy;
		bullets.push(bullet2);
	}
}

function FireMissile()
{
}

