function DrawStats()
{
	context.globalAlpha = 1;
	context.font = '18pt Courier bold'; 	
	context.fillStyle = "Red";
	
	var row = 15;
	for(var i = 0; i < msgLog.length; i++)
	{
		context.fillText(
			msgLog[i], 
			canvas.width - 300, 
			row += 35
		);
	}
}

function Draw()
{
	context.clearRect(0, 0, canvas.width, canvas.height);
	context.save();	
	context.translate(canvas.halfWidth, canvas.halfHeight);
	context.scale(zoom, zoom);	
	//SetCamera4();	
	SetCamera();
	DrawStars();
	//DrawGrid();
	//DrawBeacons();	
	DrawBullets();	
	DrawPlanets();
	DrawAstroids();
	DrawLineStrip(pathLines);
	DrawShips();	
	//DrawDebug();
	
	context.restore();
	DrawStats();
	DrawMiniMap();
}

function Draw2()
{
	context.clearRect(0, 0, canvas.width, canvas.height);
	context.save();	
	context.translate(canvas.halfWidth, canvas.halfHeight);
	
	context.scale(zoom, zoom);	
	SetCamera();
	for(var i = 0; i < ships.length; i++)
	{	
		DrawShip2(ships[i]);
		if(ships[i].target)
			DrawShip2(ships[i].target);
	}
	for(var i = 0; i < astroids.length; i++)
	{
		DrawPlanet2(astroids[i]);
	}
	
	for(var i = 0; i < planets.length; i++)
	{
		DrawPlanet2(planets[i]);
	}
	context.restore();
	DrawStats();
}

function DrawShip2(ship)
{
	var size = 10 * 1/zoom;
	length = 30 * 1/zoom;
	length2 = 10 * 1/zoom;
	
	context.save();
	context.beginPath();
	context.translate(ship.x, ship.y);
	context.moveTo(size, 0);
	context.arc(0, 0, size, 0, Math.PI * 2, false);
	context.strokeStyle = ship.color;
	context.lineWidth = 1/zoom;
	context.stroke();
	context.restore();
	DrawArrow(ship.x, ship.y, ship.vx, ship.vy, ship.color);
	
	if(ship.accX != 0 || ship.accY != 0)
	{
		DrawArrow(ship.x, ship.y, ship.accX * 10, ship.accY * 10, "yellow");
	}
	
	DrawArrow(ship.x, ship.y, 1, 1, "#66FF66", ship.angle);
	
	if(ship.obj)
	{
		//DrawArrow(ship.x, ship.obj.y, 1, 1, "FF0000", ship.angle);
		DrawPlanet2(ship.obj);
	}
}

function DrawPlanet2(planet)
{
	var size = planet.radius;
	length = 30 * 1/zoom;
	length2 = 10 * 1/zoom;
	
	context.save();
	context.beginPath();
	context.translate(planet.x, planet.y);
	context.moveTo(size, 0);
	context.arc(0, 0, size, 0, Math.PI * 2, false);
	context.strokeStyle = planet.color;
	context.lineWidth = 1/zoom;
	context.stroke();
	context.restore();
}

function DrawArrow(x, y, dx, dy, color, angle)
{
	var size = Math.sqrt(dx * dx + dy * dy) * length;
	if(!angle)
		angle = Math.atan2(dy, dx);
		
	context.save();
	context.translate(x, y);
	context.rotate(angle);
	
	context.beginPath();
	context.moveTo(0, 0);
	context.lineTo(size, 0);
	context.lineTo(size - length2, length2);
	context.moveTo(size, 0);
	context.lineTo(size - length2, -length2);
	context.strokeStyle = color;
	context.lineWidth = 1/zoom;
	context.stroke();
	context.restore();
}

function DrawDebug()
{
	//context.translate(-pathLines[pathLines.length - 1].x,  -pathLines[pathLines.length - 1].y);
	context.beginPath();
	//context.moveTo(ships[0].x + canvas.size, ships[0].y);
	//context.arc(ships[0].x, ships[0].y, canvas.size, 0, Math.PI * 2, false);
	context.rect(ships[0].x - canvas.halfWidth, ships[0].y - canvas.halfHeight, canvas.width, canvas.height);
	context.lineWidth = 10;
	context.strokeStyle = "red";
	context.stroke();
		
	context.beginPath();
	context.moveTo(ships[0].x + canvas.size2x, ships[0].y);
	context.arc(ships[0].x, ships[0].y, canvas.size2x, 0, Math.PI * 2, false);
	context.lineWidth = 10;
	context.strokeStyle = "green";
	context.stroke();	
	
	for(var i = 0; i < astroidReferences.length; i++)
	{
		context.beginPath();
		context.moveTo(astroidReferences[i].x + 20, astroidReferences[i].y);
		context.arc(astroidReferences[i].x, astroidReferences[i].y, 20, 0, Math.PI * 2, false);
		context.lineWidth = 10;
		context.strokeStyle = "red";
		context.stroke();
		
		context.fillStyle="white";
		context.font = '80pt Calibri';
		context.fillText(
			i, 
			astroidReferences[i].x, 
			astroidReferences[i].y
		);
	}
	
	for(var i = 0; i < astroids.length; i++)
	{
		DrawPlanet2(astroids[i]);
	}
	
	for(var i = 0; i < ships.length; i++)
	{
		DrawPlanet2(ships[i]);
		if(ships[i].obj)
			DrawPlanet2(ships[i].obj);
	}
	
	
}

function DrawMiniMap()
{
	context.save();	
	context.globalAlpha = 0.8;
	context.beginPath();
	context.rect(miniMapX, miniMapY, miniMapWidth, miniMapHeight);
	context.strokeStyle = "RGB(255, 0, 0)";
	context.fillStyle = "RGB(0, 0, 0)";
	context.stroke();
	context.clip();
	context.fill();
			
	context.translate(miniMapMidX, miniMapMidY);
	//context.putImageData(solarMap.data, solarMap.x, solarMap.y);
	context.scale(miniMapScale, miniMapScale);
	context.translate(-ships[0].x,  -ships[0].y);
	
	
	context.globalAlpha = 1;
	miniMapObj.x = ships[0].x - miniMapObj.width / 2;
	miniMapObj.y = ships[0].y - miniMapObj.height / 2;
	
	for(var i = 0; i < ships.length; i++)
	{		
		//if(CheckRectBounds(ships[i], miniMapObj))
		//{
			context.beginPath();
			context.rect(ships[i].x, ships[i].y, 5 / miniMapScale, 5 / miniMapScale);
			context.fillStyle = ships[i].color;
			context.fill();
		//}
	}
	
	for(var i = 0; i < planets.length; i++)
	{
		var planet = planets[i];
		
		context.save();	
		context.translate(planet.x, planet.y);
		context.rotate(planet.angle + HALFPI);
		context.drawImage(planet.img, -planet.img.halfWidth, -planet.img.halfHeight);	
		context.restore();
		
		if(planet.orbit == 0 && planet.orbitRadius > 0)
		{
			context.moveTo(planet.orbitRadius, 0);
			context.arc(0, 0, planet.orbitRadius, 0 , TWOPI, false);
			context.lineWidth = 10;
			context.strokeStyle = planet.color;
			context.stroke();
		}
	}	
	
	DrawAstroidMap();
	context.restore();
	
}

function DrawAstroidMap()
{
	for(var i = 0; i < astroidBelts.length; i++)
	{
		var astroidBelt = astroidBelts[i];
		context.beginPath();
		var radius = astroidBelt.radius + astroidBelt.halfWidth;
			
		context.moveTo(radius, 0);
		context.arc(0, 0, radius, 0 , TWOPI, false);
		context.lineWidth = 10;
		context.strokeStyle = "#888888";
		context.stroke();
		
		var radius2 = astroidBelt.radius - astroidBelt.halfWidth;
		context.moveTo(radius2, 0);
		context.arc(0, 0, radius2, 0 , TWOPI, false);
		context.lineWidth = 10;
		context.strokeStyle = "#888888";
		context.stroke();
				
		var name = "  Astroids  ";
		context.font = 'bold 220pt Calibri';
		context.textAlign = "center";
		context.fillStyle = "#888888";
		var metrics = context.measureText(name);
		var words = Math.floor((radius * TWOPI) / (metrics.width * 1.2));
		var angle = 0;
		var dAngle = TWOPI / (words);
		for(var j = 0; j < words; j++)
		{
			//for(var k = 0; k < name.length; k++)
			//{
				context.save();
				var x = Math.cos(angle) * (astroidBelt.radius - 110);
				var y = Math.sin(angle) * (astroidBelt.radius - 110);
				context.translate(x, y);
				context.rotate(angle + HALFPI);
				angle += dAngle;
				context.fillText(name, 0 ,0);
				context.restore();
			//}
		}		
	}
}

function SetCamera3()
{
	lOffsetX = SetOffset(lOffsetX, offsetX);
	lOffsetY = SetOffset(lOffsetY, offsetY);
	context.translate( -lOffsetX, -lOffsetY);
}

function SetCamera2()
{
	var offsetX = Math.cos(ships[0].angle) * offsetMax;
	var offsetY = Math.sin(ships[0].angle) * offsetMax;
	lOffsetX = SetOffset(lOffsetX, offsetX);
	lOffsetY = SetOffset(lOffsetY, offsetY);
		
	context.translate(-ships[0].x - lOffsetX, -ships[0].y - lOffsetY);
}

function SetCamera4()
{
	context.translate(-pathLines[pathLines.length - 1].x,  -pathLines[pathLines.length - 1].y);
}

function DrawShips()
{
	for(var i = 0; i < ships.length; i++)
	{
		ships[i].draw();
	}
}

function DrawPlanets()
{
	for(var i = 0; i < planets.length; i++)
	{
		planets[i].draw();
	}
}

function DrawAstroids()
{
	for(var i = 0; i < astroids.length; i++)
	{
		astroids[i].draw();
	}
}

function DrawBullets()
{	
	var removeBullets = [];
	for(var i = 0; i < bullets.length; i++)
	{
		var bullet = bullets[i];
		context.save();
		context.globalAlpha = bullet.distance / bulletTypes[bullet.index].distance;
		context.translate(bullet.x, bullet.y);
		context.beginPath();
		context.rect(0, 0, bullet.size, bullet.size);
		context.fillStyle = bullet.color;
		context.fill();
		context.restore();
		
		bullet.x += bullet.vx;
		bullet.y += bullet.vy;
		bullet.distance--;
		
		if(bullet.distance <= 0)
			removeBullets.push(i);
	}
	
	for(var i = removeBullets.length - 1; i >= 0; i--)
	{
		bullets.splice(removeBullets[i], 1);
	}
}

function DrawLineStrip(lines)
{
	if(lines.length > 1)
	{
		context.save();
		context.beginPath();
		context.moveTo(lines[0].x, lines[0].y);
		var i;
		for(i = 1; i < lines.length; i++)
		{
			context.lineTo(lines[i].x, lines[i].y);
		}
		i--;
		var dx = lines[i].x - lines[i-1].x;
		var dy = lines[i].y - lines[i-1].y;
		//var d = Math.sqrt(dx * dx + dy * dy);
		var d = Math.sqrt(ships[0].vx * ships[0].vx + ships[0].vy * ships[0].vy) * 1.8;
		var d2 = d * 2;
		
		var angle = Math.atan2(dy, dx);
		
		var nx = lines[i].x + Math.cos(angle + HALFPI) * arrow_size;
		var ny = lines[i].y + Math.sin(angle + HALFPI) * arrow_size;
		
		context.lineTo(nx, ny);	
		
		var nx1 = lines[i].x - Math.cos(angle + HALFPI) * arrow_size;
		var ny1 = lines[i].y - Math.sin(angle + HALFPI) * arrow_size;
		
		context.lineTo(nx, ny);
		
		var nx2 = lines[i].x + Math.cos(angle) * arrow_size;
		var ny2 = lines[i].y + Math.sin(angle) * arrow_size;
		
		context.lineTo(nx2, ny2);
		context.lineTo(nx1, ny1);
		context.lineTo(lines[i].x, lines[i].y);
		context.lineWidth = arrow_size;
		context.lineCap = "round";
		context.strokeStyle = "rgba(220, 220, 0, .2)";
		//context.strokeStyle = "#AAAA00";
		context.stroke();
		context.restore();
	}
}

function DrawGrid()
{
	for(x = -50000; x < 50000; x += 1000)
	{
		context.beginPath();
		context.moveTo(x, 50000);
		context.lineTo(x, -50000);
		context.strokeStyle = "white";
		context.stroke();
	}
	
	for(var y = -50000; y < 50000; y += 1000)
	{
		context.beginPath();
		context.moveTo(50000, y);
		context.lineTo(-50000, y);
		context.strokeStyle = "white";
		context.stroke();
	}
}

function DrawShip()
{
	context.save();	
	context.translate(this.x, this.y);
	context.rotate(this.angle + HALFPI);
	context.drawImage(this.img, -this.img.halfWidth, -this.img.halfHeight);	
	context.restore();
}

function SetCamera()
{
	context.translate(-ships[shipCam].x,  -ships[shipCam].y);
}

function SetOffset(lOffset, offset)
{
	if(lOffset < offset)
	{
		if(lOffset > offset - 5)
			lOffset = offset;
		else
			lOffset++;
	}
	else if (lOffset > offset )
	{
		if(lOffset < offset + 5)
			lOffset = offset;
		else
			lOffset--;		
	}
	
	return lOffset;
}

