function Initialize()
{
	document.onselectstart = function () { return false; };
	document.onContextMenu = function () { return false; };
	document.oncontextmenu = function () { return false; };
	
	canvas = document.getElementById("myCanvas");	
	context = canvas.getContext("2d");
	Resize();
	LoadImages();
}

function Resize()
{
	canvas.width = canvas.offsetWidth;
	canvas.height = canvas.offsetHeight;
	canvas.halfWidth = canvas.width / 2;
	canvas.halfHeight = canvas.height / 2;
	canvas.size = Math.sqrt(canvas.halfWidth * canvas.halfWidth + canvas.halfHeight * canvas.halfHeight);
	canvas.size2x = canvas.size * 2;
	
	if(canvas.width > canvas.height)
	{
		offsetMax = canvas.height / 3;		
	}
	else
	{
		offsetMax = canvas.width / 3;	
	}
		
	starFieldWidth = canvas.width * 2;
	starFieldHeight = canvas.height * 2;
	starFieldHalfWidth = starFieldWidth / 2;
	starFieldHalfHeight = starFieldHeight / 2;
	
	SetMiniMapSize();
	
	if(ships.length > 0)
	{
		InitStars();
	}
}

function SetMiniMapSize()
{
	miniMapWidth = canvas.width * miniMapScreenSize;
	miniMapHeight = canvas.height * miniMapScreenSize;
	miniMapX = miniMapBorder;
	miniMapY = miniMapBorder;
	miniMapMidX = miniMapWidth / 2 + miniMapX;
	miniMapMidY = miniMapHeight / 2 + miniMapY;
	miniMapObj = {
		x: miniMapX / miniMapScale,
		y: miniMapY / miniMapScale,
		width: miniMapWidth / miniMapScale,
		height: miniMapHeight / miniMapScale
	};
}

function ImagesLoaded()
{
	if(++imagesLoaded == images.length)
	{
		for(var i = 0; i < images.length; i++)
		{
			images[i].halfWidth = images[i].width / 2;
			images[i].halfHeight = images[i].height / 2;
		}
		InitGame();
	}
}

function LoadImages()
{
	LoadImage("img/spaceship_small.png");	// 0
	LoadImage("img/ship2.png");				// 1
	LoadImage("img/earth.png");				// 2
	LoadImage("img/sun.png");				// 3
	LoadImage("img/moon_small.png");		// 4
	LoadImage("img/spaceship_0.png");		// 5
	LoadImage("img/spaceship_1.png");		// 6
	LoadImage("img/spaceship_2.png");		// 7
	LoadImage("img/spaceship_3.png");		// 8
	LoadImage("img/rock_0.png");			// 9
	LoadImage("img/rock_1.png");			// 10
	LoadImage("img/rock_2.png");			// 11
	LoadImage("img/rock_3.png");			// 12
	LoadImage("img/spaceship2_0.png");		// 13
	LoadImage("img/spaceship2_1.png");		// 14
	LoadImage("img/spaceship2_2.png");		// 15
	LoadImage("img/spaceship2_3.png");		// 16
}

function LoadImage(src)
{
	var image = new Image();
	image.src = src;
	image.onload = ImagesLoaded;
	images.push(image);
}

function CreateShip(x, y, mass, thrust, color, images)
{
	var ship = {};
	ship = {};
	ship.x = x;
	ship.y = y;
	ship.vx = 0;
	ship.vy = 0;
	ship.speed = 0;
	ship.angle = 0.5;
	ship.angleDelta = 0.1;
	ship.accX = 0;
	ship.accY = 0;
	ship.accMagnitude = thrust / mass;
	ship.mass = mass;
	ship.thrust = thrust;
	ship.img = images[0];
	ship.thrustAnimTime = 1;
	ship.thrustAnimRate = 5;
	ship.images = images;
	ship.thrustAnimFrame = 1;
	ship.thrustAnimFrameStart = 1;
	ship.thrustAnimDirection = 1;
	ship.color = color;
	ship.radius = 30;
	
	ship.draw = DrawShip;
	ship.distance = 0;	
	ship.fireBullets = FireBullets;
	ship.fireMissile = FireMissile;	
	ship.bullet = bulletTypes[0];
	ship.fireTime = ship.bullet.rate;	
	ship.aiStep = 0;
	ship.aiDelay = 0;
	ship.actionAI = function () {};
	AI_ClearActions(ship);
	return ship;
}

function CreateAstroidBelt(radius, gap, density, width)
{
	var rings = Math.floor(width / canvas.size) + 1;
	var radius2 = (radius - width / 2) + (width / rings) / 2;
	for(var r = 0; r < rings; r++)
	{		
		var max = Math.round((Math.PI * 2 * radius) / gap);
		var angle = 0;
		for(var i = 0; i < max; i++)
		{
			angle += (Math.PI * 2) / max;		
			var x = Math.cos(angle) * radius2;
			var y = Math.sin(angle) * radius2;		
			
			var astroidReference = {
				x: x,
				y: y,
				radius: radius2,
				gap: gap,
				density: density,
				hidden: true, 
				width: width / rings,
				minAngle: angle - ((Math.PI * 2) / max) / 2,
				maxAngle: angle + ((Math.PI * 2) / max) / 2,
				astroids: []				
			};
			
			for(var j = 0; j < astroidReference.density; j++)
			{				
				//var x = Math.random() * astroidReference.gap - astroidReference.gap / 2 + astroidReference.x;
				//var y = Math.random() * astroidReference.gap - astroidReference.gap / 2 + astroidReference.y;
				
				var angleRange = (astroidReference.maxAngle - astroidReference.minAngle);
				var angle2 = Math.random() * angleRange + astroidReference.minAngle;
				var radius3 = Math.random() * astroidReference.width + astroidReference.radius - astroidReference.width / 2;		
				var x = Math.cos(angle2) * radius3;
				var y = Math.sin(angle2) * radius3;
				
				var image = Math.round(Math.random() * ASTROID_IMAGE_MAX) + ASTROID_IMAGE_START;
				
				var astroid = {
					x: x,
					y: y,
					img: images[image],
					angle: Math.random() * (Math.PI * 2),
					draw: DrawShip,
					reference: astroidReferences.length,
					radius: 40,
					color: "#888888",
				};
				
				astroidReference.astroids.push(astroid);
			}
			astroidReferences.push(astroidReference);			
		}
		radius2 += (width / rings);
	}
	
	astroidBelts.push( {
		radius: radius,
		width: width,
		halfWidth: width / 2,
		densidty: density
	});
}

function SetObjectInOrbit(obj, planet)
{
	var dx = obj.x - planet.x;
	var dy = obj.y - planet.y;
	var d = Math.sqrt(dx * dx + dy * dy);
	var v = Math.sqrt((G * planet.mass) / d);
	var a = Math.atan2(dy, dx);
	obj.vx = Math.cos(a - HALFPI) * v;
	obj.vy = Math.sin(a - HALFPI) * v;
}

function Run()
{
	msgLog = [];
	MovePlanets(planets);
	RunActions();
	MoveShips();
	DetectAstroids();
	CalcPath(ships[0]);
	CreateDebugLogs();
	Draw();
	t++;
	setTimeout(Run, 17);
}

function CreateSolarMap()
{
	context.save();
	context.translate(canvas.halfWidth, canvas.halfHeight);
	context.scale(miniMapScale, miniMapScale);
	var maxX = -999999999;
	var minX =  999999999;
	var maxY = -999999999;
	var minY =  999999999;
	
	for(var i = 0; i < astroidBelts.length; i++)
	{
		var astroidBelt = astroidBelts[i];
		context.beginPath();
		var radius = astroidBelt.radius + astroidBelt.halfWidth;
		
		if(radius > maxX)
			maxX = radius;
		if(-radius < minX)
			minX = -radius;
		if(radius > maxY)
			maxY = radius;
		if(-radius < minY)
			minY = -radius;
			
		context.moveTo(radius, 0);
		context.arc(0, 0, radius, 0 , TWOPI, false);
		context.lineWidth = 10;
		context.strokeStyle = "#888888";
		context.stroke();
		
		var radius2 = astroidBelt.radius - astroidBelt.halfWidth;
		context.moveTo(radius2, 0);
		context.arc(0, 0, radius2, 0 , TWOPI, false);
		context.lineWidth = 10;
		context.strokeStyle = "#888888";
		context.stroke();
				
		var name = "   A s t r o i d s   ";
		context.font = 'bold 220pt Calibri';
		context.textAlign = "center";
		context.fillStyle = "#888888";
		var metrics = context.measureText(name);
		var words = Math.floor((radius * TWOPI) / metrics.width);
		var angle = 0;
		var dAngle = TWOPI / (words * name.length);
		for(var j = 0; j < words; j++)
		{
			for(var k = 0; k < name.length; k++)
			{
				context.save();
				var x = Math.cos(angle) * (astroidBelt.radius - 110);
				var y = Math.sin(angle) * (astroidBelt.radius - 110);
				context.translate(x, y);
				context.rotate(angle + HALFPI);
				angle += dAngle;
				context.fillText(name[k], 0 ,0);
				context.restore();
			}
		}		
	}
	minX = Math.round(minX * miniMapScale);
	maxX = Math.round(maxX * miniMapScale);
	minY = Math.round(minY * miniMapScale);
	maxY = Math.round(maxY * miniMapScale);
	solarMap = {
		data: context.getImageData(minX, minY, maxX - minX, maxY - minY),
		x: minX,
		y: minY,
		width: maxX - minX,
		height: maxY - minY
	};
	
	context.restore();
}

function CheckRectBounds(obj1, obj2)
{
	return 	(obj1.x < obj2.x + obj2.width) &&
			(obj1.x > obj2.x) &&
			(obj1.y < obj2.y + obj2.height) &&
			(obj1.y > obj2.y);
}

function MovePlanets(planets)
{
	for(var i = 0; i < planets.length; i++)
	{
		var planet = planets[i];
		planet.orbitAngle += planet.orbitAngleSpeed;
		planet.x = planets[planet.orbit].x + Math.cos(planet.orbitAngle) * planet.orbitRadius;
		planet.y = planets[planet.orbit].y + Math.sin(planet.orbitAngle) * planet.orbitRadius;		
	}
}

function MoveShips()
{
	for(var i = 0; i < ships.length; i++)
	{
		var ship = ships[i];
		CalcAccleration(ship);
		ship.vx += ship.accX;
		ship.vy += ship.accY;
		ship.x += ship.vx;
		ship.y += ship.vy;
		ship.speed = Math.sqrt(ship.vx * ship.vx + ship.vy * ship.vy);
	}
}

function DetectAstroids()
{
	/*
		var astroidReference = {
			x: x,
			y: y,
			radius: radius,
			gap: gap,
			density: density,
			hidden: true, 
			width: width,
			minAngle: angle - ((Math.PI * 2) / max) / 2,
			maxAngle: angle + ((Math.PI * 2) / max) / 2
		};
	*/
	
	for(var i = 0; i < astroidReferences.length; i++)
	{
		var astroidReference = astroidReferences[i];
		var dx = ships[0].x - astroidReference.x;
		var dy = ships[0].y - astroidReference.y;
		var d = Math.sqrt(dx * dx + dy * dy);
		if(d < canvas.size2x)
		{
			if(astroidReference.hidden)
			{
				astroidReference.hidden = false;	
				for(var j = 0; j < astroidReference.astroids.length; j++)
				{
					astroids.push(astroidReference.astroids[j]);
				}
			}
		}		
		else if(astroidReference.hidden == false)
		{
			astroidReference.hidden = true;
			var removeAstroids = [];
			for(var j = 0; j < astroids.length; j++)
			{
				if(astroids[j].reference == i)
				{
					removeAstroids.push(j);
				}
			}
			
			for(var j = removeAstroids.length - 1; j >= 0; j--)
			{
				astroids.splice(removeAstroids[j], 1);
			}			
		}
	}
	//ships[0].distanceFromCenter = Math.sqrt(ships[0].x * ships[0].x + ships[0].y * ships[0].y);
}

function CalcAccleration2(ship)
{
	for(var i = 0; i < planets.length; i++)
	{	
		var dx = planets[i].x - ship.x;
		var dy = planets[i].y - ship.y;
		var d = Math.sqrt(dx * dx + dy * dy);
		var acc = (G * planets[i].mass) / (d * d);
		var angle = Math.atan2(dy , dx);
		ship.accX += Math.cos(angle) * acc;
		ship.accY += Math.sin(angle) * acc;
	}
}

//Only calculate accleration for the most powerful force
//Allows orbiting around moons
function CalcAccleration(ship)
{
	var lastAcc = 0;
	var angle = 0;
	for(var i = 0; i < planets.length; i++)
	{	
		var dx = planets[i].x - ship.x;
		var dy = planets[i].y - ship.y;
		var d = Math.sqrt(dx * dx + dy * dy);
		var acc = (G * planets[i].mass) / (d * d);
		if(acc > lastAcc)
		{
			lastAcc = acc;
			angle = Math.atan2(dy , dx);
		}		
	}	
	
	ship.accX += Math.cos(angle) * lastAcc;
	ship.accY += Math.sin(angle) * lastAcc;
}

function CalcPath(ship)
{
	pathLines = [];
	var tempShip = CopyShip(ship);
	var time = 500;
	var lastX = tempShip.x;
	var lastY = tempShip.y;
	var dTotal = 0;
	var d = 0;
	var dTracker = 0;
	//var max = (offsetMax / 3) / zoom;
	var max = (offsetMax / 3);
	var dx;
	var dy;
	
	for(var i = 0; i < time && dTotal < max; i++)
	{
		pathLines.push({x: tempShip.x, y: tempShip.y});
		tempShip.accX = 0;
		tempShip.accY = 0;
		CalcAccleration(tempShip);
		tempShip.vx += tempShip.accX;
		tempShip.vy += tempShip.accY;
		tempShip.x += tempShip.vx;
		tempShip.y += tempShip.vy;	
		dx = (tempShip.x - lastX);
		dy = (tempShip.y - lastY);
		d = Math.sqrt(dx * dx + dy * dy);
		dTotal += d;		
			
		lastX = tempShip.x;
		lastY = tempShip.y;			
	}
	if(dTotal > max)
	{
		d = dTotal - max;
		var angle = Math.atan2(dy, dx);
		var x = lastX - Math.cos(angle) * d;
		var y = lastY - Math.sin(angle) * d;
		pathLines.push({x: x, y: y});
	}
	
	//if(d <= 10)
	//	pathLines.push({x: tempShip.x, y: tempShip.y});
	offsetX = tempShip.x;
	offsetY = tempShip.y;
}

function CopyShip(ship)
{
	var newShip = {};
	newShip.x = ship.x;
	newShip.y = ship.y;
	newShip.vx = ship.vx;
	newShip.vy = ship.vy;	
	newShip.angle = ship.angle;
	newShip.mass = ship.mass;
	newShip.accX = 0;
	newShip.accY = 0;
	
	return newShip;
}

function CalcFPS()
{
	var d = new Date();
	var t = d.getTime();
	frames++;
	if(t - lastTime > 1000)
	{
		//fps = Math.round( (frames / ((t - lastTime) / 1000)) * 100) / 100;
		fps = frames / ((t - lastTime) / 1000);
		lastTime = t;
		frames = 0;
	}
	
	msgLog.push( "FPS: " + fps.toFixed(2) );
}

function CreateDebugLogs()
{
	CalcFPS();
	msgLog.push( "X: " + ships[1].x.toFixed(2) );
	msgLog.push( "Y: " + ships[1].y.toFixed(2) );
	msgLog.push( "Speed: " + ships[1].speed.toFixed(2) + " km/s" );
	msgLog.push( "Astroids: " + astroids.length );
	msgLog.push( "Astroid References: " + astroidReferences.length );
	msgLog.push( "Zoom: " + zoom );
	var dx = ships[1].target.x - ships[1].x;
	var dy = ships[1].target.y - ships[1].y;
	msgLog.push( "Distance: " + Math.sqrt( dx * dx + dy * dy).toFixed(0) );
	msgLog.push(ships[1].angle);
}