function CalculateAngle(y, x)
{
	var angle = Math.atan2(y, x);
	if(angle < 0)
		angle = TWOPI + angle;
	return angle;
}

function CalcAngle180(angle)
{
	if(angle > Math.PI)
		return angle - Math.PI;
	else
		return angle + Math.PI;
}

function DetectCollision(obj1, obj2)
{
	var dx = obj1.x - obj2.x;
	var dy = obj1.y - obj2.y;
	var d = Math.sqrt(dx * dx + dy * dy);
	
	return d < obj1.radius + obj2.radius;
}