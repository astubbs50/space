function InitGame()

{	

	planets[0] = {};

	planets[0].name = "Sun";

	planets[0].x = 0;

	planets[0].y = 0;

	planets[0].img = images[3];

	planets[0].draw = DrawShip;

	planets[0].distance = 0;

	planets[0].mass = 50000;

	planets[0].angle = -Math.PI / 2;

	planets[0].orbitRadius = 0;

	planets[0].orbit = 0;

	planets[0].orbitAngle = 0;

	planets[0].orbitAngleSpeed = 0;

	planets[0].color = "orange";

	planets[0].radius = 800;

	

	planets[1] = {};

	planets[1].name = "Earth";	

	planets[1].x = 300;

	planets[1].y = 300;

	planets[1].img = images[2];

	planets[1].draw = DrawShip;

	planets[1].distance = 0;

	planets[1].mass = 10000;

	planets[1].angle = -Math.PI / 2;

	planets[1].orbitRadius = 8000;

	planets[1].orbit = 0;

	planets[1].orbitAngle = .15;

	planets[1].orbitAngleSpeed = 0.000005;

	planets[1].color = "#6666FF";

	planets[1].radius = 500;

	

	planets[2] = {};

	planets[2].name = "Moon";	

	planets[2].x = 300;

	planets[2].y = 300;

	planets[2].img = images[4];

	planets[2].draw = DrawShip;

	planets[2].distance = 0;

	planets[2].mass = 3000;

	planets[2].angle = -Math.PI / 2;

	planets[2].orbitRadius = 1500;

	planets[2].orbit = 1;

	planets[2].orbitAngle = -.15;

	planets[2].orbitAngleSpeed = 0.0001;

	planets[2].color = "gray";

	planets[2].radius = 200;

	

	MovePlanets(planets);

	

	bulletTypes[0] = {};

	bulletTypes[0].speed = 10;

	bulletTypes[0].distance = 50;

	bulletTypes[0].color = "red";

	bulletTypes[0].size = 5;

	bulletTypes[0].halfSize = bulletTypes[0].size / 2;

	bulletTypes[0].index = 0;

	bulletTypes[0].rate = 8;

	

	ships[0] = CreateShip(8500, 1500, 10, 1, "white", [images[5], images[6], images[7], images[8]]);

	

	for(var i = 1; i < 2; i++)

	{

		var x = ships[0].x + Math.random() * 500 - 250;

		var y = ships[0].y + Math.random() * 500 - 250;

		ships[i] = CreateShip(x, y, 10, 1, "red", [images[13], images[14], images[15], images[16]]);

		

		var target = {};

		target.color = "green";

		target.x = planets[0].x + 800;

		target.y = planets[0].y + 800;

		target.vx = 0;

		target.vy = 0;

		target.stopDistance = 100;

		

		ships[i].vx = Math.random() * 5;

		ships[i].vy = Math.random() * 5;

		ships[i].vMax = 20;

		ships[i].target = target;

		ships[i].actionAI = AI_Goto;



		ships[i].target = ships[0];

		ships[i].actionAI = AI_Follow;

		

		ships[i].followDistance = 100;

		//ships[i].angleDelta = 0.02;

	}

		

	SetObjectInOrbit(ships[0], planets[1]);

	//SetObjectInOrbit(ships[1], planets[1]);

	CreateAstroidBelt(3000, 1000, 15, 500);

	CreateAstroidBelt(13000, 1000, 15, 1500);

	

	CreateSolarMap();

	InitStars();

	Run();

}