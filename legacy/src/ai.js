function AI_ClearActions(ship)
{
	ship.actionFireBullets = function () {};
	ship.actionFireMissile = function () {};
	ship.actionRotateLeft = function () {};
	ship.actionRotateRight = function () {};
	ship.actionThrust = function () {
			this.img = this.images[0];
			this.thrustAnimFrame = this.thrustAnimFrameStart;
	};	
}

function AI_Follow()
{
	DetectCollisions(this);
	msgLog.push(this.aiStep);
	var obj = this.target;
	var dx3 = obj.vx - this.vx;
	var dy3 = obj.vy - this.vy;
	var velocityDiff = Math.sqrt(dx3 * dx3 + dy3 * dy3);
	
	if(this.aiDelay > 0)
	{
		this.aiDelay--;
	}
	else
	{
		AI_ClearActions(this);
		var obj = this.target;
		var objVa = CalculateAngle(obj.vy, obj.vx);
		var objX = obj.x + Math.cos(objVa + Math.PI) * this.followDistance;
		var objY = obj.y + Math.sin(objVa + Math.PI) * this.followDistance;
		
		var dx = this.x - objX;
		var dy = this.y - objY;
		
		var targetRangeAngle = CalcAngle180(CalculateAngle(dy, dx));
		var targetRange = Math.sqrt(dx * dx + dy * dy);
		
		var dx2 = this.vx - obj.vx;
		var dy2 = this.vy - obj.vy;			
		var velocityAngle = CalcAngle180(CalculateAngle(dy2, dx2));
		
		var dx3 = obj.vx - this.vx;
		var dy3 = obj.vy - this.vy;
		var velocityDiff = Math.sqrt(dx3 * dx3 + dy3 * dy3);
		
		if(this.aiStep == 0)
		{
			var vdMax = targetRange / 2000;
			msgLog.push(vdMax);
			//if(targetRange > 10)
			//if(velocityDiff > this.accMagnitude || targetRange > 50)
			//{
				if(velocityDiff > vdMax || velocityDiff > 5)
				{
					if(velocityDiff > this.accMagnitude)
					{
						this.targetAngle = velocityAngle;
						var angleDiff = Math.abs(this.targetAngle - this.angle);
						if(angleDiff >= this.angleDelta)
						{
							AI_Cmd_SetRotation(this);
						}
						else
						{
							this.actionThrust = Thrust;
						}
					}
					else
					{
						this.targetAngle = objVa;
						var angleDiff = Math.abs(this.targetAngle - this.angle);
						if(angleDiff >= this.angleDelta)
						{
							AI_Cmd_SetRotation(this);
						}
					}
				}
				else 
				{
					this.aiStep = 1;				
				}
			//}
			//else
			//{
			//	this.targetAngle = objVa;
			//	var angleDiff = Math.abs(this.targetAngle - this.angle);
			//	if(angleDiff >= this.angleDelta)
			//	{
			//		AI_Cmd_SetRotation(this);
			//	}
					
			//}
		}
		else if(this.aiStep == 1)
		{
			this.targetAngle = targetRangeAngle;
			var angleDiff = Math.abs(this.targetAngle - this.angle);
			if(angleDiff < this.angleDelta)
			{
				this.actionThrust = Thrust;
				this.aiDelay = Math.floor(Math.sqrt(targetRange / (4 * this.accMagnitude)));				
				this.aiStep = 0;
			}
			else
			{
				AI_Cmd_SetRotation(this);
			}
		}
	}
	
	msgLog.push(velocityDiff);
	msgLog.push(this.aiDelay);
}

function AI_Goto()
{
	DetectCollisions(this);
	if(this.aiDelay > 0)
	{
		this.aiDelay--;
	}
	else
	{
		AI_ClearActions(this);
		
		var obj = this.target;
		var dx = obj.x - this.x;
		var dy = obj.y - this.y;
		var targetRange = Math.sqrt(dx * dx + dy * dy);
		
		var vMax = this.vMax;
		if(targetRange < obj.stopDistance)
			vMax = this.aiStep = 1;
			
		var approachAngle = CalculateAngle(dy, dx);
		var vx = Math.cos(approachAngle) * this.vMax;
		var vy = Math.sin(approachAngle) * this.vMax;	
		
		var dx2 = this.vx - vx;
		var dy2 = this.vy - vy;
			
		var t = 0;
		if(this.speed > 0)
			t = targetRange / this.speed;
			
		if(this.aiStep == 0)
		{
			if(this.speed < this.vMax)
			{
				if(targetRange < (.5 * this.accMagnitude * t * t) * 2)
					this.targetAngle = CalcAngle180(CalculateAngle(dy2, dx2));
				else
					this.targetAngle = CalcAngle180(CalculateAngle(this.vy, this.vx));
			}
			else
			{
				this.targetAngle = CalcAngle180(CalculateAngle(this.vy, this.vx));
			}
			
			var angleDiff = Math.abs(this.targetAngle - this.angle);
			if(angleDiff >= this.angleDelta)
			{
				AI_Cmd_SetRotation(this);
			}
			else
			{
				//if(this.speed < this.vMax)
				this.actionThrust = Thrust;
				this.aiDelay = 10;
			}
		}
		else if(this.aiStep == 1)
		{
			this.targetAngle = CalcAngle180(CalculateAngle(this.vy, this.vx));
			
			var angleDiff = Math.abs(this.targetAngle - this.angle);
			if(angleDiff >= this.angleDelta)
			{
				AI_Cmd_SetRotation(this);
			}
			else
			{
				if(this.speed > this.accMagnitude)
					this.actionThrust = Thrust;
				else
				{
					if(obj.stopDistance == 10)
					{
						if(this.oldAction)
						{
							this.actionAI = this.oldAction;
							this.target = this.oldTarget;
						}
						else
						{
							this.target.x += Math.random() * 5000 - 2500;
							this.target.y += Math.random() * 5000 - 2500;
							obj.stopDistance = 100;
							this.aiDelay = 100;
							this.vMax = 20;
						}
					}
					else
					{
						this.aiDelay = 5;
						obj.stopDistance = 10;
						this.vMax = 0.5;
					}
										
					this.aiStep = 0;
				}
			}
		}
	}
}

function AI_Cmd_SetRotation(ship)
{
	if(ship.angle < ship.targetAngle - ship.angleDelta)
	{
		if(Math.abs(ship.angle - ship.targetAngle) < Math.PI)
			ship.actionRotateRight = RotateRight;			
		else
			ship.actionRotateLeft = RotateLeft;		
	}
	else if(ship.angle > ship.targetAngle + ship.angleDelta)
	{
		if(Math.abs(ship.angle - ship.targetAngle) < Math.PI)
			ship.actionRotateLeft = RotateLeft;
		else
			ship.actionRotateRight = RotateRight;	
	}
}

function AI_SetWayPoint(ship, x, y)
{
	if(!ship.oldAction)
	{
		ship.oldTarget = ship.target;
		ship.oldAction = ship.actionAI;
	}
	
	ship.actionAI = AI_Goto;
	
	var target = {};
	target.color = "green";
	target.x = x;
	target.y = y;
	target.vx = 0;
	target.vy = 0;
	target.stopDistance = 100;
	
	ship.target = target;
	
}

function DetectCollisions(ship)
{
	//var vAngle = CalculateAngle(ship.vy, ship.vx);
	if(ship.tempTarget)
	{
		ship.actionAI = ship.oldAction;
		ship.target = ship.oldTarget;
	}
	
	for(var h = 25; h > 3; h -= 3)
	{
		var t = h * Math.floor(Math.sqrt(ship.speed / (2 * ship.accMagnitude)));
		var obj = {};
		obj.x = ship.x + ship.vx * t;
		obj.y = ship.y + ship.vy * t;
		obj.radius = ship.radius;
		obj.color = "#FF00FF";
		ship.obj = obj;
		for(var i = 0; i < astroids.length; i++)
		{
			if(DetectCollision(astroids[i], obj))
			{
				var dx = astroids[i].x - ship.x;
				var dy = astroids[i].y - ship.y;
				var angle;
				//if(Math.random() > .5)
					angle = CalculateAngle(dy, dx) + HALFPI;
				//else
				//	angle = CalculateAngle(dy, dx) - HALFPI;
				//var x2 = astroids[i].x + Math.cos(angle) * (astroids[i].radius + ship.radius) * 2;
				//var y2 = astroids[i].y + Math.sin(angle) * (astroids[i].radius + ship.radius) * 2;
				ship.vx += Math.cos(angle) * (ship.accMagnitude) * 2;
				ship.vy += Math.sin(angle) * (ship.accMagnitude) * 2;
				
				//AI_SetWayPoint(ship, x2, y2);
				//ship.tempTarget = true;
				//ship.vMax = 1;
			}
		}	
	}
}