
function MouseDown(e)
{
	if(DetectLeftButton(e))
	{
		zoom *= .5;
	}
	else
	{
		zoom /= .5;
	}
}

function DetectLeftButton(e) 
{
    if ('which' in e) 
    {
        return e.which === 1;
    } 
    else if ('buttons' in e) 
    {
        return e.buttons === 1;
    } 
    else 
    {
        return e.button === 1;
    }
}

function KeyDown(event)
{

	//alert(event.keyCode);
	switch(event.keyCode)
	{
		//1 - 0
		case 49:
		case 50:
		case 51:
		case 52:
		case 53:
		case 54:
		case 55:
		case 56:
		case 57:
		case 58:
			shipCam = event.keyCode - 49;
			break;
		//Left
		case 65:
		case 37:
		case 100:
			ships[0].actionRotateLeft = RotateLeft;
			break;
		//Right
		case 68:
		case 39:
		case 102:
			ships[0].actionRotateRight = RotateRight;
			break;
		//Up
		case 87:
		case 38:
		case 104:
			ships[0].actionThrust = Thrust;
			break;				
		//Space
		case 32:			
			ships[0].actionFireBullets = ships[0].fireBullets;
			break;
		//Enter
		case 13:			
			ships[0].actionFireMissile = ships[0].fireMissile;
			break;
		case 77:
			if(miniMapScreenSize == 0.15)
				miniMapScreenSize = 0.95;
			else
				miniMapScreenSize = 0.15;
			SetMiniMapSize();
			break;			
	}
}

function KeyUp(event)
{
	switch(event.keyCode)
	{
		//Left
		case 65:
		case 37:
		case 100:
			ships[0].actionRotateLeft = function () {};
			break;
		//Right
		case 68:
		case 39:
		case 102:
			ships[0].actionRotateRight = function () {};
			break;
		//Up
		case 87:
		case 38:
		case 104:
			ships[0].img = ships[0].images[0];
			ships[0].thrustAnimFrame = ships[0].thrustAnimFrameStart;
			ships[0].actionThrust = function () {};
			break;		
		case 32:
			ships[0].actionFireBullets = function () {};
			break;
		case 13:			
			ships[0].actionFireMissile = function () {};
			break;
	}
}


